<%@page import="java.util.ArrayList"%>
<%@page import="modelo.expediente.NotaHospitalaria"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/estiloSub.css">
<title>Nota Hospitalaria</title>
</head>
<body>
	<% ArrayList<NotaHospitalaria> notas = (ArrayList<NotaHospitalaria>)session.getAttribute("notasHospital"); %>
	<div id="Contenedor">
		<div id="headerPaciente" class="encabezado">
			<span>Ingreso al inmueble y chequeos de rutina</span>
			<hr>
		</div>
		<table id="Datos">
			<tr>
				<th>Fecha de ingreso</th>
				<th>Presion arterial</th>
				<th>Frecuencia cardiaca</th>
				<th>Frecuencia respiratoria</th>
				<th>Motivo Ingreso</th>
				<th>Diagnůstico Inicial</th>
			</tr>
			<% for (int i = 0; i < notas.size(); i++) {%>
			<tr>
				<td><%= notas.get(i).getFechaFormat() %></td>
				<td><%= notas.get(i).getPresionArterial() %></td>
				<td><%= notas.get(i).getFrecuenciaCardiaca() %></td>
				<td><%= notas.get(i).getFrecuenciaRespiratoria() %></td>
				<td><%= notas.get(i).getMotivoIngreso() %></td>
				<td><%= notas.get(i).getDiagnostico() %></td>
			</tr>
			<% } %>
		</table>
	</div>
</body>
</html>