<%@page import="modelo.usuario.Paciente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="/ExpedienteClinico/css/estiloSub.css">
<title>Cambio de Contrase�a</title>
<script>
	function vaciar()
	{
		document.getElementById("txtActual").value="";
		document.getElementById("txtNueva").value="";
		document.getElementById("txtConfirmar").value="";
	}
</script>
</head>
<body>
	<% Paciente pac = (Paciente)session.getAttribute("user"); %>
	<div id = "Contenedor">
		<form action="/ExpedienteClinico/ServletPaciente" method="post" id="frmCambioContrase�a" name="frmCambioContrase�a">
			<fieldset>
				<label for="txtActual">Contrase�a actual</label>
				<input id="txtActual" name="txtActual" type="password" maxlength="15" placeholder="Contrase�a Actual" required>
				<label for="txtNueva">Nueva contrase�a</label>
				<input id="txtNueva" name="txtNueva" type="password" maxlength="15" placeholder="Nueva Contrase�a" required>
				<label for="txtConfirmar">Confirme contrase�a nueva</label>
				<input id="txtConfirmar" name="txtConfirmar" type="password" maxlength="15" placeholder="Confirmar Contrase�a" required>
				<input id="btnActContrasena" type="submit" value="Cambiar Contrase�a">
				<input id="btnCancelar" type="button" onclick="vaciar()" value="Cancelar">
				<input name="Option" hidden="true" value="contrase�a">
			</fieldset>
		</form>
	</div>
</body>
</html>