<%@page import="modelo.expediente.Consulta"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.expediente.Expediente"%>
<%@page import="modelo.usuario.Paciente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/estiloSub.css">
<title>Consultas</title>
<% Paciente pac = (Paciente)session.getAttribute("user");
	ArrayList<Consulta> consultas = (ArrayList<Consulta>)session.getAttribute("consultas");%>
</head>
<body>
	<div id="Contenedor">
		<div id="headerPaciente" class="encabezado">
			<span>Consultas a las que ha asistido</span>
			<hr>
		</div>
		<table id="Datos">
			<tr>
				<th>Fecha de Consulta</th>
				<th>Diagn�stico</th>
				<th>C�dula del m�dico</th>
			</tr>
			<% for (int i = 0; i < consultas.size(); i++) { %>
			<tr>
				<td><%= consultas.get(i).getFechaFormat() %></td>
				<td><%= consultas.get(i).getDiagnostico() %></td>
				<td><%= consultas.get(i).getCedulaProfesional() %></td>
			</tr>
			<% } %>
		</table>
	</div>
</body>
</html>