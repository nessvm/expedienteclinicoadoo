<%@page import="modelo.expediente.NotaQuirurgica"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/estiloSub.css">
<title>Nota quir�rgica</title>
</head>
<body>
	<% ArrayList<NotaQuirurgica> notas = (ArrayList<NotaQuirurgica>)session.getAttribute("notasCirugia"); %>
	<div id="Contenedor">
		<div id="headerPaciente" class="encabezado">
			<span>Informaci�n quir�rgica</span>
			<hr>
		</div>
		<table id="Datos">
			<tr>
				<th>Fecha</th>
				<th>Diagn�stico</th>
				<th>Cirug�a</th>
				<th>Procedimiento</th>
			</tr>
			<% for(int i = 0; i < notas.size(); i++) { %>
			<tr>
				<td><%= notas.get(i).getFechaFormat() %></td>
				<td><%= notas.get(i).getDiagnostico() %></td>
				<td><%= notas.get(i).getCirugia() %></td>
				<td><%= notas.get(i).getDescripcionProcedimiento() %></td>
			</tr>
			<% } %>
		</table>
	</div>
</body>
</html>