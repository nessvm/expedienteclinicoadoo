<%@page import="modelo.usuario.Medico"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.usuario.Paciente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/estiloSub.css">
<title>Datos Personales</title>
</head>
<body>
	<% Paciente pac = (Paciente)session.getAttribute("user"); %>
	<% ArrayList<Medico> medicos = (ArrayList<Medico>)session.getAttribute("medicos"); %>
	<div id="Contenedor">
		<div id="headerPaciente" class="encabezado">
			<span>Sus datos</span>
			<hr>
		</div>
		<table id="Datos">
			<tr>
				<th>CURP</th>
				<th>Nombre</th>
				<th>Apellido Paterno</th>
				<th>Apellido Materno</th>
				<th>Telefono</th>
				<th>Correo</th>
			</tr>
			<tr>
				<td><%= pac.getCurp() %></td>
				<td><%= pac.getNombre() %></td>
				<td><%= pac.getApellidoPaterno() %></td>
				<td><%= pac.getApellidoMaterno() %></td>
				<td><%= pac.getTelefono() %></td>
				<td><%= pac.getCorreo() %></td>
			</tr>
		</table>
		<div id="headerMedico" class="encabezado">
			<span>Datos de los m�dicos a cargo de su expediente</span>
			<hr>
		</div>
		<table id="DatosMedicoAcargo">
			<tr>
				<th>C�dula Profesional</th>
				<th>Nombre</th>
				<th>Apellido Paterno</th>
				<th>Apellido Materno</th>
				<th>Tel�fono</th>
				<th>Correo electr�nico</th>
			</tr>
			<% for (int i = 0; i < medicos.size(); i++) { %>
			<tr>
				<td><%= medicos.get(i).getCedulaProfesional() %></td>
				<td><%= medicos.get(i).getNombre() %></td>
				<td><%= medicos.get(i).getApellidoPaterno() %></td>
				<td><%= medicos.get(i).getApellidoMaterno() %></td>
				<td><%= medicos.get(i).getTelefono() %></td>
				<td><%= medicos.get(i).getCorreo() %></td>
			</tr>
			<% } %>
		</table>
	</div>
</body>
</html>