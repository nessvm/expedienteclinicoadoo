<%@page import="modelo.expediente.Tratamiento"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/estiloSub.css">
<title>Tratamiento</title>
</head>
<body>
	<% ArrayList<Tratamiento> tratamientos = (ArrayList<Tratamiento>)session.getAttribute("tratamientos"); %>
	<div id="Contenedor">
		<div id="headerPaciente" class="encabezado">
			<span>Tratamientos m�dicos otorgados</span>
			<hr>
		</div>
		<table id="Datos">
			<tr>
				<th>Medicamento</th>
				<th>Dosis</th>
				<th>Horario de Suministro</th>
				<th>Inicio del Tratamiento</th>
				<th>Fin del Tratamiento</th>
			</tr>
			<% for (int i = 0; i < tratamientos.size(); i++) {%>
			<tr>
				<td><%= tratamientos.get(i).getMedicamento() %></td>
				<td><%= tratamientos.get(i).getDosis() %></td>
				<td><%= tratamientos.get(i).getFrecuencia() %></td>
				<td><%= tratamientos.get(i).getFechaInicioFormat() %></td>
				<td><%= tratamientos.get(i).getFechaFinFormat() %></td>
			</tr>
			<% } %>
		</table>
	</div>
</body>
</html>