<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/medico.css">
<link rel="stylesheet" href="../../css/bootstrap.css">
<script src="../../js/tooltip.js"></script>
<title>Nota Quirurgica</title>
</head>
<body>
	<header id="Encabezado">
		<img src="/ExpedienteClinico/img/ECE.png">
		<h1>Crear nuevo reporte de intervención</h1>
	</header>

		<section id="principal">
			<form class="form-horizontal">
				<div id="campos">
					<div class="form-group">
						<label class="col-sm-2 control-label">CURP</label>
						<div class="col-sm-10">
							<p class="form-control-static"><%=(String) session.getAttribute("curp")%></p>
						</div>
					</div>
					<div class="form-group">
						<label for="txt" class="col-sm-2 control-label">Fecha</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="txtFecha" name="txtFecha" data-toggle="tooltip" title="Haga click en el triangulo para desplegar un calendario" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtDiagnostico" class="col-sm-2 control-label">Diagnóstico</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="txtDiagnostico" name="txtDiagnostico" rows="5" required>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="txtProcedimiento" class="col-sm-2 control-label">Cirugía</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtProcedimiento" name="txtProcedimiento" placeholder="Procedimiento realizado" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtReporte" class="col-sm-2 control-label">Reporte</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="txtReporte" name="txtReporte" rows="10" required>
							</textarea>
						</div>
					</div>
					
					<input name="option" value="NotaQuirurgica" hidden="true">
					<div class="buttons">
						<button type="submit" class="btn btn-primary">Finalizar</button>
						<button type="reset" class="btn btn-default">Borrar</button>
						<button type="button" class="btn btn-default" onclick="window.location='../../ECE/Medico/menuMedico.jsp'">Cancelar</button>
					</div>
				</div>
			</form>
		</section>
</body>
</html>