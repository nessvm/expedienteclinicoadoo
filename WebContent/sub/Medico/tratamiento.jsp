<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/medico.css">
<link rel="stylesheet" href="../../css/bootstrap.css">
<script src="../../js/tooltip.js"></script>
<title>Tratamiento</title>
</head>
<body>
	<header id="Encabezado">
		<img src="/ExpedienteClinico/img/ECE.png">
		<h1>Crear nuevo tratamiento</h1>
	</header>

		<section id="principal">
			<form class="form-horizontal">
				<div id="campos">
					<div class="form-group">
						<label class="col-sm-2 control-label">CURP</label>
						<div class="col-sm-10">
							<p class="form-control-static"><%=(String) session.getAttribute("curp")%></p>
						</div>
					</div>
					<div class="form-group">
						<label for="txtMedicamento" class="col-sm-2 control-label">Medicamento</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtMedicamento" name="txtMedicamento" placeholder="Medicamento recetado" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtDosis" class="col-sm-2 control-label">Dosis</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtDosis" name="txtDosis" placeholder="Dosis indicada" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtFrecuencia" class="col-sm-2 control-label">Frecuencia</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtFrecuencia" name="txtFrecuencia" placeholder="Frecuencia de administración" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtFechaIni" class="col-sm-2 control-label">Fecha Inicio</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="txtFechaIni" name="txtFechaIni" data-toggle="tooltip" title="Haga click en el triangulo para desplegar un calendario" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtFechaFin" class="col-sm-2 control-label">Fecha Fin</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="txtFechaFin" name="txtFechaFin" data-toggle="tooltip" title="Haga click en el triangulo para desplegar un calendario" required>
						</div>
					</div>
					<input name="option" value="Tratamiento" hidden="true">
					<div class="buttons">
						<button type="submit" class="btn btn-primary">Finalizar</button>
						<button type="reset" class="btn btn-default">Borrar</button>
						<button type="button" class="btn btn-default" onclick="window.location='../../ECE/Medico/menuMedico.jsp'">Cancelar</button>
					</div>
				</div>
			</form>
		</section>
</body>
</html>