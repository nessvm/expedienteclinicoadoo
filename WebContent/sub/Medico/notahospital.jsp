<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/medico.css">
<link rel="stylesheet" href="../../css/bootstrap.css">
<script src="../../js/tooltip.js"></script>
<title>Nota Hospitalaria</title>
</head>
<body>
	<header id="Encabezado">
		<img src="/ExpedienteClinico/img/ECE.png">
		<h1>Crear nueva nota de ingreso</h1>
	</header>

		<section id="principal">
			<form class="form-horizontal">
				<div id="campos">
					<div class="form-group">
						<label class="col-sm-2 control-label">CURP</label>
						<div class="col-sm-10">
							<p class="form-control-static"><%=(String) session.getAttribute("curp")%></p>
						</div>
					</div>
					<div class="form-group">
						<label for="txt" class="col-sm-2 control-label">Fecha</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="txtFecha" name="txtFecha" data-toggle="tooltip" title="Haga click en el triangulo para desplegar un calendario" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtPresionArterial" class="col-sm-2 control-label">Presion Arterial</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtPresionArterial" name="txtPresionArterial" placeholder="XXX/XXX" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtFrecuenciaCardiaca" class="col-sm-2 control-label">Frecuencia Cardiaca</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtFrecuenciaCardiaca" name="txtFrecuenciaCardiaca" placeholder="Latidos por minuto" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtFrecuenciaRespiratoria" class="col-sm-2 control-label">Frecuencia Respiratoria</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="txtFrecuenciaRespiratoria" name="txtFrecuenciaRespiratoria" placeholder="Latidos por minuto" required>
						</div>
					</div>
					<div class="form-group">
						<label for="txtMotivoIngreso" class="col-sm-2 control-label">Motivo de Ingreso</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="txtMotivoIngreso" name="txtMotivoIngreso" rows="5" required>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="txtDiagnostico" class="col-sm-2 control-label">Diagnůstico</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="txtDiagnostico" name="txtDiagnostico" rows="5" required>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="txtEstadoNutricional" class="col-sm-2 control-label">Estado Nutricional</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="txtEstadoNutricional" name="txtEstadoNutricional" rows="5" required>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="txtFamilia" class="col-sm-2 control-label">Familia</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="txtFamilia" name="txtFamilia" rows="5" required>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="txtOtros" class="col-sm-2 control-label">Observaciones</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="txtOtros" name="txtOtros" rows="5" required>
							</textarea>
						</div>
					</div>
					
					<input name="option" value="NotaHospitalaria" hidden="true">
					<div class="buttons">
						<button type="submit" class="btn btn-primary">Finalizar</button>
						<button type="reset" class="btn btn-default">Borrar</button>
						<button type="button" class="btn btn-default" onclick="window.location='../../ECE/Medico/menuMedico.jsp'">Cancelar</button>
					</div>
				</div>
			</form>
		</section>
</body>
</html>