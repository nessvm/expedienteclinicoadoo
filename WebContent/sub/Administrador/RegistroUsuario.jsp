<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro nuevo usuario</title>
<script src="../../js/funcRegistro.js"></script>
<script src="../../js/validacion.js"></script>
<link rel="stylesheet" href="../../css/estiloSubAdmin.css">
</head>
<body>
	<div id="Contenedor">
		<form action="../../ServletCRUD" method="post" name="frmRegUsuario" id="frmRegUsuario">
			<div id="lateral">
				<fieldset id="perfilUsr">
					<legend>Perfil Usuario</legend>
					<select id="cbTipoUsr" name="cbTipoUsr">
						<option value="" selected="selected">Tipo de usuario</option>
						<option value="Paciente">Paciente</option>
						<option value="Medico">M�dico</option>
					</select>
					<select id="cbEspecialidad" name="cbEspecialidad" style="display: none;">
						<option value="">Especialidad</option>
					</select>
				</fieldset>
				<fieldset id="cuentaUsr">
					<legend>Datos Cuenta</legend>
					<input id="txtEmail" class="txtGrande" name="txtEmail" type="email" placeholder="Correo Electr�nico  (requerido)" required>
					<input id="txtContrasena" class="txtGrande" name="txtContrasena" type="password" maxlength="15" placeholder="Contrase�a  (requerido)" required>
					<input id="txtContrasena2" class="txtGrande" name="txtContrasena2" type="password" maxlength="15" placeholder="Confirmar Contrase�a  (requerido)" required>
					<input id="txtPerfil" name="txtPerfil" type="hidden">
				</fieldset>
			</div>
			<fieldset id="datosPersonales">
				<legend>Datos Personales</legend>
				<input id="txtNombre" class="txtGrande" name="txtNombre" title="SIN ACENTOS. Ejemplo Jose | Jose Alberto" type="text" pattern="[A-Za-z]{3,20}|([A-Za-z]{3,15}( )?[A-Za-z]{3,})" maxlength="20" placeholder="Nombre  (requerido)" required>
				<input id="txtApaterno" class="txtGrande" name="txtApaterno" title="SIN ACENTOS. Ejemplo Martinez" type="text" pattern="[A-Za-z]{3,20}|([A-Za-z]{2,} [A-Za-z]{2,}( )?[A-Za-z]{2,})" maxlength="20" placeholder="Apellido Paterno  (requerido)" required>
				<input id="txtAmaterno" class="txtGrande" name="txtAmaterno" title="SIN ACENTOS. Ayala" type="text" pattern="[A-Za-z]{3,20}|([A-Za-z]{2,} [A-Za-z]{2,}( )?[A-Za-z]{2,})" maxlength="20" placeholder="Apellido Materno  (requerido)" required>
				<input id="txtTel" class="txtGrande" name="txtTel" type="text" title="Ejemplo para Tel:56082413 Ejemplo para Cel:5559911501" pattern="([0-9]{8})|([0-9]{10})" maxlength="10" placeholder="Telefono  (requerido)" required>
				<input id="txtCalle" class="txtGrande" name="txtCalle" type="text" maxlength="30" placeholder="Calle">
				<input id="txtNumExt" class="chico" name="txtNumExt" type="text" pattern="[0-9]{0,4}" maxlength="4" placeholder="# Exterior">
				<input id="txtNumInt" class="chico" name="txtNumInt" type="text" title="Ejemplo: 1234 | Mz 1 Lt 8" maxlength="10" placeholder="# Interior">
				<input id="txtCP" class="chico" name="txtCP" type="text" pattern="[0-9]{5}" maxlength="5" placeholder="C.P.">
			</fieldset>
			<div id="Botones">
				<input id="btnCancelar" name="btnCancelar" type="button" value="Cancelar" onclick="location='../../ECE/Administrador/menuAdministrador.jsp'">
				<input id="btnAccion" name="btnAccion" type="button" value="Registrar usuario">
			</div>
		</form>
	</div>
</body>
</html>