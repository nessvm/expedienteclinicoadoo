<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro Administrador</title>
<link rel="stylesheet" href="../../css/estiloSubAdmin.css">
</head>
<body>
	<div id="Contenedor">
		<section id="cajaRegistro">
			<form action="../../ServletCRUD" method="post" id="frmRegAdmin" name="frmRegAdmin">
				<input id="txtUsuario" name="txtUsuario" type="text" required placeholder="Usuario (Requerido)">
				<input id="txtContrasena" name="txtContrasena" type="password" required placeholder="Contraseņa (Requerido)">
				<input id="txtContrasena2" name="txtContrasena2" type="password" required placeholder="Verifique contraseņa (Requerido)">
				<input id="txtPerfil" name="txtPerfil" type="hidden" value="3">
				<input id="btnCancelar" name="btnCancelar" type="button" value="Cancelar" onclick="location='../../ECE/Administrador/menuAdministrador.jsp'">
				<input id="btnAccion" name="btnAccion" type="submit" value="Registrar">
			</form>
		</section>
	</div>
</body>
</html>