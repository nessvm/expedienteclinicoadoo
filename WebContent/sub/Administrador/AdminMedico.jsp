<%@page import="java.util.ArrayList"%>
<%@page import="modelo.usuario.Medico"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrar m�dico</title>
<link rel="stylesheet" href="../../css/estiloSubAdmin.css">
</head>
<body>
	<% ArrayList<Medico> medicos = (ArrayList<Medico>)session.getAttribute("medicos"); %>
	<div id="Contenedor">
		<div id="cajaTabla">
			<header>Gesti�n de m�dicos</header>
			<hr>
			<table id="tablaUsuarios">
				<tr>
					<th>C�dula Profesional</th>
					<th>Nombre</th>
					<th>Apellido Paterno</th>
					<th>Apellido Materno</th>
					<th>Tel�fono</th>
					<th>Correo electr�nico</th>
					<th>Consultar</th>
					<th>Actualizar</th>
					<th>Eliminar</th>
				</tr>
				<% for (int i = 0; i < medicos.size(); i++) { %>
				<tr>
					<td><%= medicos.get(i).getCedulaProfesional() %></td>
					<td><%= medicos.get(i).getNombre() %></td>
					<td><%= medicos.get(i).getApellidoPaterno() %></td>
					<td><%= medicos.get(i).getApellidoMaterno() %></td>
					<td><%= medicos.get(i).getTelefono() %></td>
					<td><%= medicos.get(i).getCorreo() %></td>
					<td><img src="../../img/search.png"></td>
					<td><img src="../../img/refresh.png"></td>
					<td><img src="../../img/delete.png"></td>
				</tr>
				<% } %>
			</table>
		</div>
	</div>
</body>
</html>