<%@page import="java.util.ArrayList"%>
<%@page import="modelo.usuario.Paciente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/estiloSubAdmin.css">
<title>Administrar paciente</title>
</head>
<body>
	<% ArrayList<Paciente> pacientes = (ArrayList<Paciente>)request.getSession().getAttribute("pacientes");  %>
	<div id="Contenedor">
		<div id="cajaTabla">
			<header>Gestión de pacientes</header>
			<hr>
			<table id="tablaUsuarios">
				<tr>
					<th>CURP</th>
					<th>Nombre</th>
					<th>Apellido Paterno</th>
					<th>Apellido Materno</th>
					<th>Telefono</th>
					<th>Correo</th>
					<th>Contraseña</th>
					<th>Direccion</th>
					<th>Consultar</th>
					<th>Actualizar</th>
					<th>Eliminar</th>
				</tr>
				<% for(int i = 0; i < pacientes.size(); i++)
				{ 
					String direccion = "Calle " + pacientes.get(i).getCalle() + " num. interior " + pacientes.get(i).getNumeroExterior() + " num. interior "
					+ " C.P. " + pacientes.get(i).getCodigoPostal();%>
				<tr>
					<td><% pacientes.get(i).getCurp(); %></td>
					<td><% pacientes.get(i).getNombre(); %></td>
					<td><% pacientes.get(i).getApellidoPaterno(); %></td>
					<td><% pacientes.get(i).getApellidoMaterno(); %></td>
					<td><% pacientes.get(i).getTelefono(); %></td>
					<td><% pacientes.get(i).getCorreo(); %></td>
					<td><% pacientes.get(i).getContraseña(); %></td>
					<td><% out.println(direccion); %></td>
					<td><img src="../../img/search.png"></td>
					<td><img src="../../img/refresh.png"></td>
					<td><img src="../../img/delete.png"></td>
				</tr>
				<% }%>
			</table>
		</div>
	</div>
</body>
</html>