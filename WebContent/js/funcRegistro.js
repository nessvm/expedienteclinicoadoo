function iniciar()
{
	cbUsuario = document.getElementById("cbTipoUsr");
	cbUsuario.addEventListener("change", crearElemento, false);
}

function crearElemento()
{
	var usuario = cbUsuario.value;
	var contenedor = document.getElementById("cuentaUsr");
	if(usuario!="")
	{
		var textbox = document.createElement("input");
		contenedor.appendChild(textbox);
		if(usuario=="Paciente")
		{
			document.getElementById("cbEspecialidad").style.display="none";
			if((document.getElementById("txtCedula"))!=null)
			{
				contenedor.removeChild(document.getElementById("txtCedula"));
				contenedor.removeChild(document.getElementById("txtAutorizado"));
			}
			textbox.setAttribute("id","txtCURP");
			textbox.setAttribute("name","txtCURP");
			textbox.setAttribute("class","txtGrande");
			textbox.setAttribute("type","text");
			textbox.setAttribute("pattern","[A-Z]{4}[0-9]{6}(H|M)[A-Z]{5}[0-9]{2}");
			textbox.setAttribute("maxLength","18");
			textbox.setAttribute("placeholder","CURP  (requerido)");
			textbox.setAttribute("required", "true");
			obtenerPerfil(usuario);
		}
		else
		{
			document.getElementById("cbEspecialidad").style.display="block";
			if((document.getElementById("txtCURP"))!=null)
			{
				contenedor.removeChild(document.getElementById("txtCURP"));
			}
			var autorizado = document.createElement("input");
			contenedor.appendChild(autorizado);
			textbox.setAttribute("id","txtCedula");
			textbox.setAttribute("class","txtGrande");
			textbox.setAttribute("name","txtCedula");
			textbox.setAttribute("type","text");
			textbox.setAttribute("pattern","[0-9]{7}");
			textbox.setAttribute("maxLength","7");
			textbox.setAttribute("placeholder","C�dula Profesional  (requerido)");
			textbox.setAttribute("required", "true");
			
			autorizado.setAttribute("id","txtAutorizado");
			autorizado.setAttribute("name","txtAutorizado");
			autorizado.setAttribute("type","hidden");
			autorizado.setAttribute("value","1");
			obtenerPerfil(usuario);
		}
	}
	else
	{
		document.getElementById("cbEspecialidad").style.display="none";
		obtenerPerfil(usuario);
		if((document.getElementById("txtCURP"))!=null)
			contenedor.removeChild(document.getElementById("txtCURP"));
		if((document.getElementById("txtCedula"))!=null)
		{
			contenedor.removeChild(document.getElementById("txtCedula"));
			contenedor.removeChild(document.getElementById("txtAutorizado"));
		}
			
	}
}

function obtenerPerfil(perfil)
{
	var txtperfil = document.getElementById("txtPerfil");
	if(perfil == "")
	{
		txtperfil.removeAttribute("value");
	}
	else
	{
		if(perfil=="Paciente")
		{
			txtperfil.setAttribute("value", "1");
		}
		else
		{
			txtperfil.setAttribute("value","2");
		}
	}
}
window.addEventListener("load",iniciar,false);