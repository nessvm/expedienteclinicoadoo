$(document).ready(function(){
	$("#Menu li").click(function(){
		direccion = "";
		if(this.getAttribute("id") == "btnDatos")
			direccion = "sub/Paciente/DatosPaciente.jsp";
		else if(this.getAttribute("id") == "btnConsulta")
			direccion = "sub/Paciente/ConsultaPaciente.jsp";
		else if(this.getAttribute("id") == "btnTratamiento")
			direccion = "sub/Paciente/TratPaciente.jsp";
		else if(this.getAttribute("id") == "btnNotaHospitalaria")
			direccion = "sub/Paciente/notaHpaciente.jsp";
		else if(this.getAttribute("id") == "btnNotaQuirurgica")
			direccion = "sub/Paciente/notaQpaciente.jsp";
		else
			direccion = "sub/Paciente/conPaciente.jsp";
		$("#InfoConsulta").fadeOut("slow",cargarPagina);
		function cargarPagina(){
			$("#InfoConsulta").attr("src",direccion).load(function(){
				$(this).fadeIn("slow");});
		}
	 });
});