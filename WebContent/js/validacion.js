window.addEventListener("load",iniciar,false);

function iniciar()
{
	var boton = document.getElementById("btnAccion");
	boton.addEventListener("click", enviar, false);
	document.frmRegUsuario.addEventListener("input",validacion,false);
}

function validacion(e)
{
	var elemento = e.target;
	if(elemento.validity.valid)
	{
		if(elemento.getAttribute("class") == "txtGrande" || elemento.getAttribute("class") == "bien" || elemento.getAttribute("class") == "error")
			elemento.setAttribute("class","bien");
		else
			elemento.setAttribute("class","bienChico");
	}
	else
	{
		if(elemento.getAttribute("class") == "txtGrande" || elemento.getAttribute("class") == "bien" || elemento.getAttribute("class") == "error")
			elemento.setAttribute("class","error");
		else
			elemento.setAttribute("class","errorChico");
	}
}
function enviar()
{
	var cbTipo = document.getElementById("cbTipoUsr");
	var con = document.getElementById("txtContrasena");
	var con2 = document.getElementById("txtContrasena2");
	var valido = document.frmRegUsuario.checkValidity();
	if(valido)
		if(cbTipo.value!="")
			if(con.value == con2.value)
				document.frmRegUsuario.submit();
			else
				alert("ERROR. Verifique que las contraseñas coincidan");
		else
			alert("Error. Seleccione un tipo de usuario");
	else
		alert("ERROR. Verifique su información");
}