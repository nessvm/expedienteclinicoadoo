<%@page import="modelo.usuario.Administrador"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Men� Administrador</title>
<link rel="stylesheet" href="../../css/estiloMadmin.css"/>

<% Administrador admin = (Administrador)request.getSession().getAttribute("user"); %>
</head>
<body>
	<div id="Contenedor">
		<header id="Encabezado">
			<div id="cajaLogo">
				<img src="../../img/ECE.png">
			</div>
			<div id="cajaDatosAdmin">
				<hgroup>
					<h2>Bienvenido</h2>
					<h3>Administrador:<span> <%= admin.getIdAdministrador() %> </span></h3>
				</hgroup>
			</div>
			<div id="cajaSesion">
				<h3><a href="../../index.jsp" onclick="<% session.invalidate(); %>">Cerrar Sesi�n</a></h3>
			</div>
		</header>
		<section id="contenedorMenu">
			<ul id="menuAdmin">
				<li id="btnAgregar" onclick="location='../../sub/Administrador/RegistroUsuario.jsp'">
					<span>P</span>
					<h2>Agregar nuevo usuario</h2>
					<h3>Dar de alta a un paciente o m�dico</h3>
				</li>
				<li id="btnAgregar" onclick="location='../../sub/Administrador/RegistroAdmin.jsp'">
					<span>U</span>
					<h2>Agregar administrador</h2>
					<h3>Dar de alta a un administrador</h3>
				</li>
				<li id="btnAdminPaciente" onclick="location='../../sub/Administrador/AdminPaciente.jsp'">
					<span>A</span>
					<h2>Administrar paciente</h2>
					<h3>Consultar, Actualizar y Eliminar Pacientes</h3>
				</li>
				<li id="btnAdminMedico" onclick="location='../../sub/Administrador/AdminMedico.jsp'">
					<span>N</span>
					<h2>Administrar m�dico</h2>
					<h3>Consultar, Actualizar y Eliminar M�dicos</h3>
				</li>
			</ul>
		</section>
	</div>
	<footer id="Pie">
		<div></div>
	</footer>
	<div id="Derechos"><cite>&copy; 2013-2017 Todos los derechos reservados a TI Wesc&reg;</cite></div>
</body>
</html>