<%@page import="modelo.usuario.Medico"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../../css/estiloMmedico.css">
<title>Men� M�dico</title>
<script>
	function enviar(option)
	{
		var valido = document.frmCurp.checkValidity();
		if(valido){
			var request = "../../ServletMedico?curp=" +
					document.getElementById("txtCurp").value +
					"&&option=" + option;
					
			window.location.assign(request);
		}
		else
			if(document.getElementById("txtCurp").value=="")
				alert("ERROR. Campo requerido. No puede ir vac�o");
			else
				alert("ERROR. Verifique el curp que introdujo\nEjemplo: AAAA123456HDFABC12");
	}
</script>

<% Medico medico = (Medico)session.getAttribute("user");%>
</head>
<body>
	<header id="Encabezado">
		<div id="cajaLogo">
			<img src="/ExpedienteClinico/img/ECE.png">
		</div>
		<div id="cajaDatosMedico">
			<hgroup>
				<h2>Bienvenido</h2>
				<h3>M�dico:<span> <%= medico.getNombre() + " " + medico.getApellidoPaterno() %> </span></h3>
			</hgroup>
		</div>
		<div id="cajaSesion">
			<h3><a href="../../index.jsp">">Cerrar sesi�n</a></h3>
		</div>
	</header>
	<section id="cajaMenu">
		<ul id="menu">
			<li onclick="enviar('consulta');">
				<span>&#0033</span>
				<div>
					<h2 class="titulo">Crear nueva consulta</h2>
					<h4 class="desc">Nuevo reporte de consulta cl�nica</h4>
				</div>
			</li>
			<li onclick="enviar('notahospital');">
				<span>&#0109</span>
				<div>
					<h2 class="titulo">Crear nota hospitalaria</h2>
					<h4 class="desc">Nueva nota de ingreso</h4>
				</div>
			</li>
			<li onclick="enviar('notaquirurgica');">
				<span>&#0084</span>
				<div>
					<h2 class="titulo">Crear nota quir�rgica</h2>
					<h4 class="desc">Nuevo reporte de cirug�a</h4>
				</div>
			</li>
			<li onclick="enviar('tratamiento');">
				<span>&#0089</span>
				<div>
					<h2 class="titulo">Asignar tratamiento</h2>
					<h4 class="desc">Nuevas indicaciones por medicamento</h4>
				</div>
			</li>
			<li onclick="enviar('expediente');">
				<span>&#0035</span>
				<div>
					<h2 class="titulo">Consultar expediente</h2>
					<h4 class="desc">Consulte el expediente de su paciente</h4>
				</div>
			</li>
		</ul>
		<hr>
		<form action="#" id="frmCurp" name="frmCurp">
			<label for="txtCurp">CURP paciente:</label>
			<% if (session.getAttribute("curp") == null) { %>
			<input id="txtCurp" name="txtCurp" type="text" placeholder="CURP paciente" title="Solo son v�lidas las may�sculas" pattern="[A-Z]{4}[0-9]{6}(H|M)[A-Z]{5}[0-9]{2}" maxlength="18" required><br><br><br>
			<% } else {  %>
			<input id="txtCurp" name="txtCurp" type="text" placeholder="CURP paciente" title="Solo son v�lidas las may�sculas" pattern="[A-Z]{4}[0-9]{6}(H|M)[A-Z]{5}[0-9]{2}" maxlength="18" required value=<%= (String)session.getAttribute("curp") %>><br><br><br>
			<% 	}
				String message;
				if ((message = (String)session.getAttribute("message")) != null) { %>
				<h2 style="font-family: sans-serif; color: white;"><%= message %></h2>
			<% 	session.setAttribute("message", null); 
				} %>
		</form>
	</section>
</body>
</html>