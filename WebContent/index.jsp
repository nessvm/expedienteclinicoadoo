<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Autenticaci�n Usuario</title>
	<link rel="stylesheet" href="css/estiloLogin.css">
	<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="js/animacion.js"></script>
</head>
<body>
	<div id="contenedor">
		<header id="encabezado">
			<ul>
				<li><img src="img/ECE.png"></li>
				<li id="descPag"><h1>Log in</h1></li>
			</ul>
		</header>
		<section id="login">
			<article id="cajaLogin">
				<header> <h1>Acceso al sistema</h1> </header>
				<form action="ServletLogin" method="post" id="frmLogin">
					<label for="txtUsuario" id="lblUsuario"> Usuario</label>
					<input class="textbox" name="txtUsuario" type="text" maxlength="23" placeholder="Usuario" autocomplete="off">
					<label for="txtContrasena" id="lblContrasena"> Contrase�a</label>
					<input class="textbox" name="txtContrasena" type="password" maxlength="23" placeholder="Contrase�a">
					<a id="btnInstrucciones">Instrucciones de registro</a>
					<input class="botonLogin" name="btnEntrar" type="submit" value="Login">
				</form>
			</article>
			<article id="cajaInstrucciones">
				<header>Instrucciones de registro</header>
				<p>1. Si usted es un derechohabiente tendr� que asistir a la cl�nica correspondiente con identificaci�n oficial.</p>
				<p>2. En el �rea de recursos humanos ser� registrado por uno de los administradores del sistema.</p>
				<p>3. Se le pedir� informaci�n personal para almacenarla en una base de datos donde tendr�mos su registro para posteriores consultas.</p>
				<p>4. A partir de ese momento usted podr� accesar al sistema para poder ver informaci�n a cerca de su expediente cl�nico y dem�s datos que se 
					van recaudando de cada consulta a la que ha asistido.</p>
				<p>5. Para poder accesar a la informaci�n de su expediente m�dico, el sistema le pedir� su <strong>CURP</strong> como punto de identificaci�n y 
					una contrase�a que usted le proporcionar� al administrador que se encuentre a cargo de su registro.</p>
				<p>6. Si en algun momento olvida su contrase�a de click en el v�nculo que dice <strong>�Olvido su contrase�a?</strong> se le enviar� un mensaje
					a la direcci�n de correo electr�nico que nos ha proporcionado con sus datos y su contrase�a.</p>
				<p>1. Si usted es m�dico acuda con el administrador de su unidad hospitalaria para que lo puedan registrar en el sistema.</p>
				<p>2. Se le pedir� informaci�n personal para almacenarla en una base de datos donde tendr�mos su registro.</p>
				<p>3. A partir de este momento usted podr� accesar al sistema para ver informaci�n a cerca de los pacientes que tiene asignados y podr� accesar
					a su expediente cl�nico para anotar observaciones, diagn�sticos, tratamientos, etc.</p>
				<p>4. Para poder accesar a la informaci�n mencionada anteriormente el sistema le pedir� como punto de identificaci�n su <strong>C�dula Profesional</strong>
					y una contrase�a que el administrador le asignar�.</p>
				<p>5. Si en algun momento olvida su contrase�a de click en el v�nculo que dice <strong>�Olvido su contrase�a?</strong> se le enviar� un mensaje
					a la direcci�n de correo electr�nico que nos ha proporcionado con sus datos y su contrase�a.</p>
			</article>
		</section>
		<footer id="pie">
		</footer>
	</div>
</body>
</html>