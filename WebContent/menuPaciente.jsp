<%@page import="modelo.usuario.Paciente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/estiloMpaciente.css">
<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="js/funMpac.js"></script>
<title>Men� Paciente</title>
</head>
<body>

	<% Paciente pac = (Paciente)session.getAttribute("user"); %>
	<header id="Encabezado">
		<div id="cajaLogo">
			<img src="/ExpedienteClinico/img/ECE.png">
		</div>
		<div id="cajaDatosPaciente">
			<hgroup>
				<h2>Bienvenido</h2>
				<h3>
					Paciente:<span> <%= pac.getNombre() + " " + pac.getApellidoPaterno() %>
					</span>
				</h3>
			</hgroup>
		</div>
		<div id="cajaSesion">
			<h3>
				<a href="/ExpedienteClinico/index.jsp">Cerrar sesi�n</a>
			</h3>
		</div>
	</header>
	<aside id="MenuPaciente">
		<ul id="Menu">
			<li id="btnDatos" title="Muestra sus datos personales"><img
				src="/ExpedienteClinico/img/paciente.png">
				<h2>Identidad</h2></li>
			<li id="btnConsulta" title="Informe sobre sus consultas"><img
				src="/ExpedienteClinico/img/consulta.png">
				<h2>Consultas</h2></li>
			<li id="btnTratamiento" title="�Qu� tratamientos ha adquirido?">
				<img src="/ExpedienteClinico/img/pillPot.png">
				<h2>Tratamientos</h2>
			</li>
			<li id="btnNotaHospitalaria" title="Chequeos de rutina"><img
				src="/ExpedienteClinico/img/notaHospital.png">
				<h2>Condiciones</h2></li>
			<li id="btnNotaQuirurgica" title="Informe sobre sus cirugias"><img
				src="/ExpedienteClinico/img/notaQuirurgica.png">
				<h2>Cirugias</h2></li>
			<li id="btnContrasena" title="Modifique su contrase�a"><img
				src="/ExpedienteClinico/img/password.png">
				<h2>Contrase�a</h2></li>
		</ul>
		<br />
		<div id="agregarMedico">
			<form action="ServletPaciente" method="get">
				<label for="txtCedProf">�No ve a su m�dico en el expediente?</label><br>
				<input id="txtCedProf" name="txtCedProf" type="text"
					class="form-control" placeholder="C�dula Profesional"> <input
					type="submit" value="Enviar solicitud" class="submit">
				<p>Agr�guelo apuntando su c�dula profesional en este campo y
					presione "Enviar solicitud"</p>
			</form>
		</div>
	</aside>



	<section id="frameInfo">
		<iframe id="InfoConsulta"
			src="/ExpedienteClinico/sub/Paciente/DatosPaciente.jsp" width=100%
			height="470px"> </iframe>
		<% 	String message = null;
				if ((message = (String)session.getAttribute("message")) != null) {%>
		<h2 style="text-align: center"><%= message %></h2>
		<% session.setAttribute("message", null); }%>
	</section>
</body>
</html>