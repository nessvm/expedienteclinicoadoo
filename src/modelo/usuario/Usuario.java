package modelo.usuario;

public abstract class Usuario extends DBUser {
	protected String contraseña;
	protected String nombre;
	protected String apellidoPaterno;
	protected String apellidoMaterno;
	protected String calle;
	protected String numeroExterior;
	protected String numeroInterior;
	protected String codigoPostal;
	protected String telefono;
	protected String correo;
	protected boolean valido;

	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public String getContraseña() {
		return contraseña;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public String getCalle() {
		return calle;
	}

	public String getNumeroExterior() {
		return numeroExterior;
	}

	public String getNumeroInterior() {
		return numeroInterior;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public boolean isValido() {
		return valido;
	}

	public abstract void setContraseña(String contraseña);

	public abstract void setNombre(String nombre);

	public abstract void setApellidoPaterno(String apellidoPaterno);

	public abstract void setApellidoMaterno(String apellidoMaterno);

	public abstract void setCalle(String calle);

	public abstract void setNumeroExterior(String numeroExterior);

	public abstract void setNumeroInterior(String numeroInterior);

	public abstract void setCodigoPostal(String codigoPostal);

	public abstract void setTelefono(String telefono);

	public abstract void setCorreo(String correo);

	public abstract void setValido(boolean valido);
	
	

}
