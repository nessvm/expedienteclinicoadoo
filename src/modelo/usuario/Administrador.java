
package modelo.usuario;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;

import javax.sql.DataSource;


public class Administrador extends DBUser {

	private String idAdministrador;
	private String contraseña;
	private DataSource ds;
	private boolean valido;

	public Administrador(DataSource ds, String idAdministrador) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "select * from ECE.Administrador where idAdministrador = '" + idAdministrador +"';"; //Construimos la instruccion que queremos enviar.
			ResultSet res = sentencia.executeQuery(query);  //Ejecutamos la instruccion. 

			if(res.next()){  //recuperamos la informacion del paciente si el id existe...
				this.idAdministrador = res.getString("idAdministrador");
				this.valido = true;
			}
			else{  //Si el id no existe...
				System.out.println("No existe el id del administrador.");
				this.valido = false;
				return;
			}
			con.close();
		}
		catch(SQLTimeoutException e){
			e.printStackTrace();
			return;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public Administrador(DataSource ds, String idAdministrador, String contraseña)
	{					//insertar registros en la base de datos.
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = String.format("INSERT INTO `ECE`.`Administrador` (`idAdministrador`, `contraseña`,"
					+ " `Perfil_Usuario_idPerfil`) VALUES ('%s', '%s', 3);",
					idAdministrador, contraseña);

			int filas = sentencia.executeUpdate(query);

			if(filas == 1){
				this.idAdministrador = idAdministrador;
				this.valido = true;
			}
			else{
				System.out.println("Error al insertar Administrador.");
			}
			con.close();
		}
		catch(SQLTimeoutException e){
			e.printStackTrace();
			return;
		}
		catch(SQLException e){
			e.printStackTrace();
		}

	}
	public String getIdAdmin(){
		return idAdministrador;
	}

	public void setContraseña(String contraseña){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update Administrador set contraseña = '" + contraseña
					+ "' where idAdministrador = '" + idAdministrador + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.contraseña = contraseña;
			}	
			else{
				System.out.println("Error al actualizar la contraseña del Administrador " + idAdministrador);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public boolean eliminarAdministrador(){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "delete from Administrador where idAdministrador = '"
					+ this.idAdministrador + "';";
			int filasAfectadas = sentencia.executeUpdate(query);

			if (filasAfectadas == 1)
				return true;
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
		} catch (SQLException e){
			System.out.println("Excepción SQL con el constructor de extracción");
			e.printStackTrace();
		}
		return false;
	}
	public DataSource getDataSource() {
		return ds;
	}
	public void setDataSource(DataSource ds) {
		this.ds = ds;
	}
	public String getIdAdministrador() {
		return idAdministrador;
	}
	public String getContraseña() {
		return contraseña;
	}
	public boolean isValido() {
		return valido;
	}


}