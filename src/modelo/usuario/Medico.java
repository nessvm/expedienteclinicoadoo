package modelo.usuario;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;

import javax.sql.DataSource;

public class Medico extends Usuario {
	private String cedulaProfesional;
	private boolean autorizado;

	/**
	 * @author Ness
	 * creado: 17/11/2013 22:24
	 * 
	 * Constructor que extrae la informaci�n del m�dico de la base de datos, usando la 
	 * llave primaria @param cedulaProfesional, despu�s se llenan todos los atributos 
	 * del objeto con la informaci�n obtenida.
	 */
	public Medico(DataSource ds, String cedulaProfesional) {
		try{
			
			this.ds = ds;
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "select * from ECE.Medico where cedulaProfesional = '" + cedulaProfesional +"';";
			ResultSet resultado = sentencia.executeQuery(query);
			
			if (resultado.next()){
				this.cedulaProfesional = resultado.getString("cedulaProfesional");
				this.contrase�a = resultado.getString("contrase�a");
				this.nombre = resultado.getString("nombre");
				this.apellidoPaterno = resultado.getString("apellidoPaterno");
				this.apellidoMaterno = resultado.getString("apellidoMaterno");
				this.calle = resultado.getString("calle");
				this.numeroExterior = resultado.getString("numeroExterior");
				this.numeroInterior = resultado.getString("numeroInterior");
				this.codigoPostal = resultado.getString("codigoPostal");
				this.telefono = resultado.getString("telefono");
				this.correo = resultado.getString("correo");
				this.autorizado = resultado.getBoolean("autorizado");
				this.valido = true;
			}
			else {
				System.out.println("Error al extraer al m�dico: " + cedulaProfesional);
				this.valido = false;
				return;
			}
			con.close();
			
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Ness
	 * creado: 18/11/2013 00:35
	 * 
	 * Constructor que inserta la informaci�n del m�dico a la base de datos, toma como
	 * par�metros todos los atributos de la tabla m�dico, en caso de que no haya errores
	 * con la sentencia SQL, se asignan los par�metros dados al constructor a los atri-
	 * butos del objeto
	 */
	
public Medico(DataSource ds, String cedulaProfesional, String contrase�a, String nombre,
			String apellidoPaterno, String apellidoMaterno, String calle, String numeroExterior,
			String numeroInterior, String codigoPostal, String telefono, String correo,
			boolean autorizado){
		
		String auth;
		if(autorizado)
			auth = "1";
		else
			auth = "0";
		
		try{	
			this.ds = ds;
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "INSERT INTO `ECE`.`Medico` (`cedulaProfesional`, `contrase�a`, `nombre`,"
					+ " `apellidoPaterno`, `apellidoMaterno`, `calle`, `numeroExterior`, `numeroInterior`,"
					+ " `codigoPostal`, `telefono`, `correo`, `autorizado`, `Perfil_Usuario_idPerfil`)"
					+ " VALUES ('"
					+ cedulaProfesional + "', '" 
					+ contrase�a + "', '" 
					+ nombre + "', '"
					+ apellidoPaterno + "', '"
					+ apellidoMaterno + "', '"
					+ calle + "', '"
					+ numeroExterior + "', '"
					+ numeroInterior + "', '"
					+ codigoPostal + "', '"
					+ telefono + "', '"
					+ correo + "', "
					+ auth
					+ ", 2);";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.cedulaProfesional = cedulaProfesional;
				this.contrase�a = contrase�a;
				this.nombre = nombre;
				this.apellidoPaterno = apellidoPaterno;
				this.apellidoMaterno = apellidoMaterno;
				this.calle = calle;
				this.numeroExterior = numeroExterior;
				this.numeroInterior = numeroInterior;
				this.codigoPostal = codigoPostal;
				this.telefono = telefono;
				this.correo = correo;
				this.autorizado = autorizado;
				this.valido = true;
			}
			
			else {
				System.out.println("Error al insertar al m�dico " + cedulaProfesional + " en la BD");
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de inserci�n");
			e.printStackTrace();
		}
	}
	
	public String getCedulaProfesional() {
		return cedulaProfesional;
	}

	public boolean isAutorizado() {
		return autorizado;
	}

	@Override
	public void setContrase�a(String contrase�a) {
		
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set contrase�a = '" + contrase�a
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.contrase�a = contrase�a;
			}
			
			else{
				System.out.println("Error al actualizar la contrase�a del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar la contrase�a del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	@Override
	public void setNombre(String nombre) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set nombre = '" + nombre
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.nombre = nombre;
			}
			
			else{
				System.out.println("Error al actualizar el nombre del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el nombre del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	@Override
	public void setApellidoPaterno(String apellidoPaterno) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set apellidoPaterno = '" + apellidoPaterno
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.apellidoPaterno = apellidoPaterno;
			}
			
			else{
				System.out.println("Error al actualizar el apellido paterno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido paterno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	@Override
	public void setApellidoMaterno(String apellidoMaterno) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set apellidoMaterno = '" + apellidoMaterno
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.apellidoMaterno = apellidoMaterno;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	@Override
	public void setCalle(String calle) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set calle = '" + calle
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.calle = calle;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	@Override
	public void setNumeroExterior(String numeroExterior) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set numeroExterior = '" + numeroExterior
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.numeroExterior = numeroExterior;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}

	}

	@Override
	public void setNumeroInterior(String numeroInterior) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set numeroInterior = '" + numeroInterior
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.numeroInterior = numeroInterior;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	@Override
	public void setCodigoPostal(String codigoPostal) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set codigoPostal = '" + codigoPostal
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.codigoPostal = codigoPostal;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}

	}

	@Override
	public void setTelefono(String telefono) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set telefono = '" + telefono
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.telefono = telefono;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}

	}

	@Override
	public void setCorreo(String correo) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set correo = '" + correo
					+ "' where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.correo = correo;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	public void setAutorizado(boolean autorizado) {
		String auth;
		if (autorizado)
			auth = "1";
		else
			auth = "0";
		
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update medico set autorizado = " + auth
					+ " where cedulaProfesional = '" + cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1){
				this.autorizado = autorizado;
			}
			
			else{
				System.out.println("Error al actualizar el apellido materno del m�dico " + cedulaProfesional);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
			return;
		} catch (SQLException e){
			System.out.println("Excepci�n SQL al actualizar el apellido materno del m�dico " + cedulaProfesional);
			e.printStackTrace();
		}
	}

	@Override
	public void setValido(boolean valido) {
		this.valido = valido;
	}
	
	public boolean eliminarMedico(){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "delete from medico where cedulaProfesional = '"
					+ this.cedulaProfesional + "';";
			int filasAfectadas = sentencia.executeUpdate(query);
			
			if (filasAfectadas == 1)
				return true;
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
		} catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			e.printStackTrace();
		}
		return false;
	}

}

