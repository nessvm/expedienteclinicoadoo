package modelo.usuario;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;

import javax.sql.DataSource;

public class Paciente extends Usuario{

	private String curp;

	public Paciente(DataSource ds, String curp) {
		try{
			this.ds = ds;
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement(); //Crea un objeto para enviar instrucciones a la base de datos
			String query = "select * from ECE.Paciente where curp = '" + curp +"';"; //Construimos la instruccion que queremos enviar.
			ResultSet res = sentencia.executeQuery(query);  //Ejecutamos la instruccion. 

			if(res.next()){  //recuperamos la informacion del paciente si el curp existe...
				this.curp = res.getString("curp");
				this.contrase�a = res.getString("contrase�a");
				this.nombre = res.getString("nombre");
				this.apellidoPaterno = res.getString("apellidoPaterno");
				this.apellidoMaterno = res.getString("apellidoMaterno");
				this.calle = res.getString("calle");
				this.numeroExterior = res.getString("numeroExterior");
				this.numeroInterior = res.getString("numeroInterior");
				this.codigoPostal = res.getString("codigoPostal");
				this.telefono = res.getString("telefono");
				this.correo = res.getString("correo");
				this.valido = true;
			}
			else{  //Si el curp no existe...
				System.out.println("No existe el curp.");
				this.valido = false;
				return;
			}
			con.close();
		}
		catch(SQLTimeoutException e){
			e.printStackTrace();
			return;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

	public Paciente(DataSource ds, String curp, String contrase�a, String nombre, String apellidoPaterno, String apellidoMaterno, 
			String calle, String numeroExterior, String numeroInterior, String codigoPostal, String telefono, String correo)
	{					//insertar registros en la base de datos.
		try{
			this.ds = ds;
			Connection con = ds.getConnection();;
			Statement sentencia = con.createStatement();
			String query = String.format("INSERT INTO `ECE`.`Paciente` "
					+ "(`curp`, `contrase�a`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `calle`,"
					+ " `numeroExterior`, `numeroInterior`, `codigoPostal`, `telefono`, `correo`,"
					+ " `Perfil_Usuario_idPerfil`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 1);",
					curp, contrase�a, nombre, apellidoPaterno, apellidoMaterno, calle, numeroExterior,
					numeroInterior, codigoPostal, telefono, correo);

			int filas = sentencia.executeUpdate(query);

			if(filas == 1){
				this.curp = curp;
				this.nombre = nombre;
				this.apellidoPaterno = apellidoPaterno;
				this.apellidoMaterno = apellidoMaterno;
				this.calle = calle;
				this.numeroExterior = numeroExterior;
				this.numeroInterior = numeroInterior;
				this.codigoPostal = codigoPostal;
				this.telefono = telefono;
				this.correo = correo;
				this.valido = true;
			}
			else{
				System.out.println("Error al insertar paciente.");
			}
			con.close();
		}
		catch(SQLTimeoutException e){
			e.printStackTrace();
			return;
		}
		catch(SQLException e){
			e.printStackTrace();
		}

	}

	public String getCurp(){
		return curp;
	}

	public void setContrase�a(String contrase�a){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set contrase�a = '" + contrase�a
					+ "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.contrase�a = contrase�a;
			}	
			else{
				System.out.println("Error al actualizar la contrase�a del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void setNombre(String nombre){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set nombre = '" + nombre + "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.nombre = nombre;
			}	
			else{
				System.out.println("Error al actualizar el nombre del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}

	}

	public void setApellidoPaterno(String apellidoPaterno){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set apellidoPaterno = '" + apellidoPaterno+ "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.apellidoPaterno = apellidoPaterno;
			}	
			else{
				System.out.println("Error al actualizar el apellido paterno del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	public void setApellidoMaterno(String apellidoMaterno){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set apellidoMaterno = '" + apellidoMaterno+ "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.apellidoMaterno = apellidoMaterno;
			}	
			else{
				System.out.println("Error al actualizar el apellido materno del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}
	}


	public void setCalle(String calle) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set calle = '" + calle + "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.calle = calle;
			}	
			else{
				System.out.println("Error al actualizar la calle del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}

	}


	public void setNumeroExterior(String numeroExterior) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set numeroExterior = '" + numeroExterior + "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.numeroExterior = numeroExterior;
			}	
			else{
				System.out.println("Error al actualizar el numero exterior del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}
	}


	public void setNumeroInterior(String numeroInterior) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set numeroInterior = '" + numeroInterior + "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.numeroInterior = numeroInterior;
			}	
			else{
				System.out.println("Error al actualizar el numero interior del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}

	}

	public void setCodigoPostal(String codigoPostal) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set codigoPostal = '" + codigoPostal + "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.codigoPostal = codigoPostal;
			}	
			else{
				System.out.println("Error al actualizar el c�digo postal del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}

	}


	public void setTelefono(String telefono) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set telefono = '" + telefono + "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.telefono = telefono;
			}	
			else{
				System.out.println("Error al actualizar el tel�fono del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void setCorreo(String correo) {
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "update paciente set correo = '" + correo + "' where curp = '" + curp + "';";
			int filas = sentencia.executeUpdate(query);

			if (filas == 1){
				this.correo = correo;
			}	
			else{
				System.out.println("Error al actualizar el correo del paciente " + curp);
				return;
			}
			con.close();
		} catch (SQLTimeoutException e){
			e.printStackTrace();
			return;
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public boolean eliminarPaciente(){
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			String query = "delete from paciente where curp = '"
					+ this.curp + "';";
			int filasAfectadas = sentencia.executeUpdate(query);

			if (filasAfectadas == 1)
				return true;
			con.close();
		} catch (SQLTimeoutException e){
			System.out.println("El tiempo de respuesta del servidor MySQL se ha excedido");
		} catch (SQLException e){
			System.out.println("Excepci�n SQL con el constructor de extracci�n");
			e.printStackTrace();
		}
		return false;
	}

}