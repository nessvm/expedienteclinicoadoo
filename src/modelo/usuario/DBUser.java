package modelo.usuario;


import javax.sql.DataSource;

public abstract class DBUser {
	protected DataSource ds;
	
	public DBUser() {
		// TODO Auto-generated constructor stub
	}

	public DataSource getDataSource() {
		return ds;
	}

	public void setDataSource(DataSource ds) {
		this.ds = ds;
	}
	

}
