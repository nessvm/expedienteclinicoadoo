package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import modelo.expediente.Consulta;
import modelo.expediente.Expediente;
import modelo.expediente.NotaHospitalaria;
import modelo.expediente.NotaQuirurgica;
import modelo.expediente.Tratamiento;

public class GenericCRUD {
	private DataSource ds;

	public GenericCRUD(DataSource ds) {
		this.ds = ds;
	}

	/*
	 * Select ----------------------------------------------------
	 */

	/**
	 * 
	 * @param id
	 *            La llave primaria de la instancia que se extraer� de la BD
	 * @param type
	 *            El tipo de objeto que se extraer� de la BD
	 * @return El objeto de tipo {@literal type} que se haya extra�do, o en caso
	 *         de que no exista;
	 */
	public Object selectObject(String id, String type) {
		switch (type) {

		case "Expediente":
			return selectExpediente(id);
		case "NotaQuirurgica":
			return selectNotaQuirurgica(id);
		case "NotaHospitalaria":
			return selectNotaHospitalaria(id);
		case "Tratamiento":
			return selectTratamiento(id);
		case "Consulta":
			return selectConsulta(id);
		default:
			return null;
		}
	}

	private Object selectConsulta(String id) {
		Consulta consulta;
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;

		query = "SELECT * FROM ECE.Consulta WHERE idConsulta = ?;";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setInt(1, Integer.parseInt(id));
			result = stmnt.executeQuery();

			if (result.next()) {
				consulta = new Consulta();
				consulta.setCedulaProfesional(result
						.getString("cedulaProfesional"));
				consulta.setDiagnostico(result.getString("diagnostico"));
				consulta.setFecha(result.getDate("fecha"));
				consulta.setId(Integer.parseInt(id));
				consulta.setIdExpediente(result.getInt("idExpediente"));

				con.close();
				return consulta;
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return null;
	}

	private Object selectTratamiento(String id) {
		Tratamiento tratamiento;
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;

		query = "SELECT * FROM ECE.Tratamiento WHERE idTratamiento = ?;";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setInt(1, Integer.parseInt(id));
			result = stmnt.executeQuery();

			if (result.next()) {
				tratamiento = new Tratamiento();
				tratamiento.setDosis(result.getString("dosis"));
				tratamiento.setFechaFin(result.getDate("fechaFin"));
				tratamiento.setFechaInicio(result.getDate("fechaInicio"));
				tratamiento.setFrecuencia(result.getString("frecuencia"));
				tratamiento.setId(Integer.parseInt(id));
				tratamiento.setIdExpediente(result.getInt("idExpediente"));
				tratamiento.setMedicamento(result.getString("medicamento"));

				con.close();
				return tratamiento;
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return null;
	}

	private Object selectNotaHospitalaria(String id) {
		NotaHospitalaria nota;
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;

		query = "SELECT * FROM ECE.NotaHospitalaria WHERE idNotaHospitalaria = ?;";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setInt(1, Integer.parseInt(id));
			result = stmnt.executeQuery();

			if (result.next()) {
				nota = new NotaHospitalaria();
				nota.setId(Integer.parseInt(id));
				nota.setFecha(result.getDate("fecha"));
				nota.setPresionArterial(result.getString("presionArterial"));
				nota.setFrecuenciaCardiaca(result
						.getString("frecuencuaCardiaca"));
				nota.setFrecuenciaRespiratoria(result
						.getString("frecuenciaRespiratoria"));
				nota.setMotivoIngreso(result.getString("motivoIngreso"));
				nota.setDiagnostico(result.getString("diagnostico"));
				nota.setEstadoNutricional(result.getString("estadoNutricional"));
				nota.setFamilia(result.getString("familia"));
				nota.setOtros(result.getString("otros"));
				nota.setIdExpediente(result.getInt("idExpediente"));

				con.close();
				return nota;
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return null;
	}

	private Object selectNotaQuirurgica(String id) {
		NotaQuirurgica nota;
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;

		query = "SELECT * FROM ECE.NotaQuirurgica WHERE idNotaQuirurgica = ?;";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setInt(1, Integer.parseInt(id));
			result = stmnt.executeQuery();

			if (result.next()) {
				nota = new NotaQuirurgica();
				nota.setId(Integer.parseInt(id));
				nota.setDiagnostico(result.getString("diagnostico"));
				nota.setCirugia(result.getString("cirugia"));
				nota.setDescripcionProcedimiento(result
						.getString("descripcionProcedimiento"));
				nota.setIdExpediente(result.getInt("idExpediente"));
				nota.setFecha(result.getDate("fecha"));

				con.close();
				return nota;
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return null;
	}

	private Object selectExpediente(String id) {
		Expediente expediente;
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;

		query = "SELECT * FROM ECE.Expediente WHERE curp = ?;";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setString(1, id);
			result = stmnt.executeQuery();

			if (result.next()) {
				expediente = new Expediente();
				expediente.setIdExpediente(result.getInt("idExpediente"));
				expediente.setCurp(result.getString("curp"));

				con.close();
				return expediente;
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * Insert -------------------------------------------------
	 */

	public int insertObject(Object obj) {
		if (obj instanceof Expediente) {
			return insertExpediente((Expediente) obj);
		} else if (obj instanceof NotaQuirurgica) {
			return insertNotaQuirurgica((NotaQuirurgica) obj);
		} else if (obj instanceof NotaHospitalaria) {
			return insertNotaHospitalaria((NotaHospitalaria) obj);
		} else if (obj instanceof Tratamiento) {
			return insertTratamiento((Tratamiento) obj);
		} else if (obj instanceof Consulta) {
			return insertConsulta((Consulta) obj);
		} else
			return -1;
	}

	private int insertConsulta(Consulta consulta) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		int rows, generatedID;
		String query;

		query = "INSERT INTO `ECE`.`Consulta` (`fecha`, `diagnostico`, `idExpediente`, `cedulaProfesional`) VALUES (?, ?, ?, ?);";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			stmnt.setString(1, sdf.format(consulta.getFechaAsDate()));
			stmnt.setString(2, consulta.getDiagnostico());
			stmnt.setInt(3, consulta.getIdExpediente());
			stmnt.setString(4, consulta.getCedulaProfesional());
			rows = stmnt.executeUpdate();

			if (rows == 1) {
				result = stmnt.getGeneratedKeys();
				if(result.next()){
					generatedID = result.getInt(1);
					
					con.close();
					return generatedID;
				}
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return -1;
	}

	private int insertTratamiento(Tratamiento tratamiento) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		int rows, generatedID;
		String query;

		query = "INSERT INTO `ECE`.`Tratamiento` (`medicamento`, `dosis`, `frecuencia`, `fechaInicio`, `fechaFin`, `idExpediente`) VALUES (?, ?, ?, ?, ?, ?);";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);			
			stmnt.setString(1, tratamiento.getMedicamento());
			stmnt.setString(2, tratamiento.getDosis());
			stmnt.setString(3, tratamiento.getFrecuencia());
			stmnt.setDate(4, tratamiento.getFechaInicioAsDate());
			stmnt.setDate(5, tratamiento.getFechaFinAsDate());
			stmnt.setInt(6, tratamiento.getIdExpediente());
			rows = stmnt.executeUpdate();

			if (rows == 1) {
				result = stmnt.getGeneratedKeys();
				if(result.next()){
					generatedID = result.getInt(1);
					
					con.close();
					return generatedID;
				}
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return -1;
	}

	private int insertNotaHospitalaria(NotaHospitalaria nota) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		int rows, generatedID;
		String query;

		query = "INSERT INTO `ECE`.`NotaHospitalaria` (`fecha`, `presionArterial`, `frecuenciaCardiaca`, `frecuenciaRespiratoria`, `motivoIngreso`, `diagnostico`, `estadoNutricional`, `familia`, `otros`, `idExpediente`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setDate(1, nota.getFechaAsDate());
			stmnt.setString(2, nota.getPresionArterial());
			stmnt.setString(3, nota.getFrecuenciaCardiaca());
			stmnt.setString(4, nota.getFrecuenciaRespiratoria());
			stmnt.setString(5, nota.getMotivoIngreso());
			stmnt.setString(6, nota.getDiagnostico());
			stmnt.setString(7, nota.getEstadoNutricional());
			stmnt.setString(8, nota.getFamilia());
			stmnt.setString(9, nota.getOtros());
			stmnt.setInt(10, nota.getIdExpediente());
			rows = stmnt.executeUpdate();

			if (rows == 1) {
				result = stmnt.getGeneratedKeys();
				if(result.next()){
					generatedID = result.getInt(1);
					
					con.close();
					return generatedID;
				}
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return -1;
	}

	private int insertNotaQuirurgica(NotaQuirurgica nota) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		int rows, generatedID;
		String query;

		query = "INSERT INTO `ECE`.`NotaQuirurgica` (`diagnostico`, `cirugia`, `descripcionProcedimiento`, `idExpediente`, `fecha`) VALUES (?, ?, ?, ?, ?);";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setString(1, nota.getDiagnostico());
			stmnt.setString(2, nota.getCirugia());
			stmnt.setString(3, nota.getDescripcionProcedimiento());
			stmnt.setInt(4, nota.getIdExpediente());
			stmnt.setDate(5, nota.getFechaAsDate());
			rows = stmnt.executeUpdate();

			if (rows == 1) {
				result = stmnt.getGeneratedKeys();
				if(result.next()){
					generatedID = result.getInt(1);
					
					con.close();
					return generatedID;
				}
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return -1;
	}

	private int insertExpediente(Expediente expediente) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		int rows, generatedID;
		String query;

		query = "INSERT INTO `ECE`.`Expediente` (`curp`) VALUES (?);";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setString(1, expediente.getCurp());
			rows = stmnt.executeUpdate();

			if (rows == 1) {
				result = stmnt.getGeneratedKeys();
				if(result.next()){
					generatedID = result.getInt(1);
					
					con.close();
					return generatedID;
				}
			}

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return -1;
	}

}
