/**
 * @(#)conexionGenerica.java
 *
 *
 * @author 
 * @version 1.00 2013/11/11
 */
package modelo.dao;

import java.sql.*;

public abstract class conexionGenerica {

	private String usuario = null;
	private String password = null;
	private String driver = null;
	private String urlConexion = null;
	private Connection conexion = null;
	private PreparedStatement preparar = null;
	private ResultSet resultado = null; 

	public conexionGenerica(String usuario, String password, String driver, String urlConexion) {
		super();
		this.usuario = usuario;
		this.password = password;
		this.driver = driver;
		this.urlConexion = urlConexion;
	}

	public Connection open(){

		try{
			Class.forName(driver);
			this.conexion = DriverManager.getConnection(this.urlConexion, this.usuario, this.password);
		}
		catch(SQLException e){
			System.out.println(e);
		}
		catch(ClassNotFoundException e){
			System.out.println("Driver no encontrado: " + e);
		}
		return this.conexion;
	}


	//Se utiliza para realizar los insert, update o delete a los registros

	public boolean ejecutaUpdate (String cmd){
		if (cmd != null)
			try {
				this.preparar = this.conexion.prepareStatement(cmd);
				this.preparar.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e);
			}

		else
			System.out.println("String SQL statement value is null");

		return true;
	}

	/**
	 * obtiene los registros de una consulta mediante un result set para que puedan
	 * ser manejados a discrecion del usuario.
	 */
	public ResultSet ejecutaQuery (String cmd){
		System.out.println("��"+cmd);
		if (cmd != null)
			try {
				this.preparar = this.conexion.prepareStatement(cmd);
				this.resultado = this.preparar.executeQuery(); 
			} catch (SQLException e) {
				System.out.println(e);
			}  
		return this.resultado;
	}

	/**
	 * Permite cerrar la coneccion SQL.
	 */

	public void close(){
		try {
			if (this.conexion != null){
				this.conexion.clearWarnings();
				this.conexion.close();
			}
		} catch (SQLException e) {
			this.conexion = null;
			System.out.println(e);
		}
	}



	public String getUrlConexion() {
		return urlConexion;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public Connection getConeccion() {
		return conexion;
	}
	public void setUrlConeccion(String urlConexion) {
		this.urlConexion = urlConexion;
	}
	protected PreparedStatement getPreparar() {
		return preparar;
	}
	protected void setPreparar(PreparedStatement preparar) {
		this.preparar = preparar;
	}

	protected ResultSet getResultado() {
		return resultado;
	}
	protected void setResultado(ResultSet resultado) {
		this.resultado = resultado;
	}

}