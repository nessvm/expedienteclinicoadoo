package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import modelo.expediente.Consulta;
import modelo.expediente.Expediente;
import modelo.expediente.NotaHospitalaria;
import modelo.expediente.NotaQuirurgica;
import modelo.expediente.Tratamiento;
import modelo.usuario.Medico;

public class UserDAO {
	private DataSource ds;

	public UserDAO(DataSource ds) {
		this.ds = ds;
	}

	public ArrayList<Medico> getMedicos(String curp) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;
		ArrayList<Medico> medicos;

		query = "SELECT * FROM ECE.PacienteMedico WHERE Paciente_curp = ?;";

		try {
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			stmnt.setString(1, curp);
			result = stmnt.executeQuery();
			medicos = new ArrayList<>();

			while (result.next()) {
				medicos.add(new Medico(ds, result.getString(2)));
			}
			con.close();
			return medicos;

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<Consulta> getConsultas(String curp) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;
		ArrayList<Consulta> consultas;
		GenericCRUD keeper = new GenericCRUD(ds);
		Expediente expediente;

		query = "SELECT * FROM ECE.Consulta WHERE idExpediente = ?;";

		try {
			consultas = new ArrayList<>();
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			expediente = (Expediente) keeper.selectObject(curp, "Expediente");
			stmnt.setInt(1, expediente.getIdExpediente());
			result = stmnt.executeQuery();

			while (result.next()) {
				consultas
						.add((Consulta) keeper.selectObject(
								String.valueOf(result.getInt("idConsulta")),
								"Consulta"));
			}
			con.close();
			return consultas;

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<Tratamiento> getTratamientos(String curp) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;
		ArrayList<Tratamiento> tratamientos;
		GenericCRUD keeper = new GenericCRUD(ds);
		Expediente expediente;

		query = "SELECT * FROM ECE.Tratamiento WHERE idExpediente = ?;";

		try {
			tratamientos = new ArrayList<>();
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			expediente = (Expediente) keeper.selectObject(curp, "Expediente");
			stmnt.setInt(1, expediente.getIdExpediente());
			result = stmnt.executeQuery();

			while (result.next()) {
				tratamientos.add((Tratamiento) keeper.selectObject(
						String.valueOf(result.getInt("idTratamiento")),
						"Tratamiento"));
			}
			con.close();
			return tratamientos;

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<NotaHospitalaria> getNotasHospital(String curp) {
		Connection con;
		PreparedStatement stmnt;
		ResultSet result;
		String query;
		ArrayList<NotaHospitalaria> notas;
		GenericCRUD keeper = new GenericCRUD(ds);
		Expediente expediente;

		query = "SELECT * FROM ECE.NotaHospitalaria WHERE idExpediente = ?;";

		try {
			notas = new ArrayList<>();
			con = ds.getConnection();
			stmnt = con.prepareStatement(query);
			expediente = (Expediente)keeper.selectObject(curp, "Expediente");
			stmnt.setInt(1, expediente.getIdExpediente());
			result = stmnt.executeQuery();

			while (result.next()) {
				notas.add((NotaHospitalaria)keeper.selectObject(String.valueOf(result.getInt("idNotaHospitalaria")), "NotaHospitalaria")); 
			}
			con.close();
			return notas;

		} catch (SQLException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		
		return null;
	}

		public ArrayList<NotaQuirurgica> getNotasCirugia(String curp) {
			Connection con;
			PreparedStatement stmnt;
			ResultSet result;
			String query;
			ArrayList<NotaQuirurgica> notas;
			GenericCRUD keeper = new GenericCRUD(ds);
			Expediente expediente;

			query = "SELECT * FROM ECE.NotaQuirurgica WHERE idExpediente = ?;";

			try {
				notas = new ArrayList<>();
				con = ds.getConnection();
				stmnt = con.prepareStatement(query);
				expediente = (Expediente)keeper.selectObject(curp, "Expediente");
				stmnt.setInt(1, expediente.getIdExpediente());
				result = stmnt.executeQuery();

				while (result.next()) {
					notas.add((NotaQuirurgica)keeper.selectObject(String.valueOf(result.getInt("idNotaQuirurgica")), "NotaQuirurgica")); 
				}
				con.close();
				return notas;

			} catch (SQLException e) {
				System.out.println(e.toString());
				e.printStackTrace();
			}

			return null;
		}

}
