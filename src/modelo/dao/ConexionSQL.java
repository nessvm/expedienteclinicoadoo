/**
 * @(#)ConexionMySQL.java
 *
 *
 * @author 
 * @version 1.00 2013/11/11
 */

package modelo.dao;


public class ConexionSQL extends conexionGenerica {


    public ConexionSQL(String user, String password, String url) {
    	super(user, password, "com.mysql.jdbc.Driver", "jdbc:mysql://" + url);
    }
}