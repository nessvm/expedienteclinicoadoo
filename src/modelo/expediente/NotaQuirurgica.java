package modelo.expediente;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;

public class NotaQuirurgica {
	private int id;
	private String diagnostico;
	private String cirugia;
	private String descripcionProcedimiento;
	private int idExpediente;
	private Calendar fecha;
	public NotaQuirurgica(int id, String diagnostico, String cirugia,
			String descripcionProcedimiento, int idExpediente, Calendar fecha) {
		super();
		this.id = id;
		this.diagnostico = diagnostico;
		this.cirugia = cirugia;
		this.descripcionProcedimiento = descripcionProcedimiento;
		this.idExpediente = idExpediente;
		this.fecha = fecha;
	}
	public NotaQuirurgica() {
		super();
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the diagnostico
	 */
	public String getDiagnostico() {
		return diagnostico;
	}
	/**
	 * @return the cirugia
	 */
	public String getCirugia() {
		return cirugia;
	}
	/**
	 * @return the descripcionProcedimiento
	 */
	public String getDescripcionProcedimiento() {
		return descripcionProcedimiento;
	}
	/**
	 * @return the idExpediente
	 */
	public int getIdExpediente() {
		return idExpediente;
	}
	/**
	 * @return the fecha
	 */
	public Calendar getFecha() {
		return fecha;
	}
	
	public Date getFechaAsDate(){
		return (Date) fecha.getTime();
	}
	
	public String getFechaFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(fecha.getTime());
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param diagnostico the diagnostico to set
	 */
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	/**
	 * @param cirugia the cirugia to set
	 */
	public void setCirugia(String cirugia) {
		this.cirugia = cirugia;
	}
	/**
	 * @param descripcionProcedimiento the descripcionProcedimiento to set
	 */
	public void setDescripcionProcedimiento(String descripcionProcedimiento) {
		this.descripcionProcedimiento = descripcionProcedimiento;
	}
	/**
	 * @param idExpediente the idExpediente to set
	 */
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha.setTime(fecha);
	}
	
}
