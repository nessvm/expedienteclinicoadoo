package modelo.expediente;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Consulta {
	private int id;
	private Calendar fecha;
	private String diagnostico;
	private int idExpediente;
	private String cedulaProfesional;

	public Consulta() {
		this.fecha = Calendar.getInstance();
	}

	public Consulta(int id, Calendar fecha, String diagnostico,
			int idExpediente, String cedulaProfesional) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.diagnostico = diagnostico;
		this.idExpediente = idExpediente;
		this.cedulaProfesional = cedulaProfesional;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the fecha
	 */
	public Calendar getFecha() {
		return fecha;
	}
	
	public Date getFechaAsDate() {
		return fecha.getTime();
	}
	
	public String getFechaFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(fecha.getTime());
	}

	/**
	 * @return the diagnostico
	 */
	public String getDiagnostico() {
		return diagnostico;
	}

	/**
	 * @return the idExpediente
	 */
	public int getIdExpediente() {
		return idExpediente;
	}

	/**
	 * @return the cedulaProfesional
	 */
	public String getCedulaProfesional() {
		return cedulaProfesional;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha.setTime(fecha);
	}

	/**
	 * @param diagnostico the diagnostico to set
	 */
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	/**
	 * @param idExpediente the idExpediente to set
	 */
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}

	/**
	 * @param cedulaProfesional the cedulaProfesional to set
	 */
	public void setCedulaProfesional(String cedulaProfesional) {
		this.cedulaProfesional = cedulaProfesional;
	}
	
	

}
