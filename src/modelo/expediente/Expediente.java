package modelo.expediente;

public class Expediente {
	private int idExpediente;
	private String curp;

	public Expediente() {
		super();
	}

	public Expediente(int idExpediente, String curp) {
		super();
		this.idExpediente = idExpediente;
		this.curp = curp;
	}

	/**
	 * @return the idExpediente
	 */
	public int getIdExpediente() {
		return idExpediente;
	}

	/**
	 * @return the curp
	 */
	public String getCurp() {
		return curp;
	}

	/**
	 * @param idExpediente
	 *            the idExpediente to set
	 */
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}

	/**
	 * @param curp
	 *            the curp to set
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}

}
