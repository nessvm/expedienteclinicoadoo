package modelo.expediente;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NotaHospitalaria {
	private int id;
	private Calendar fecha;
	private String presionArterial;
	private String frecuenciaCardiaca;
	private String frecuenciaRespiratoria;
	private String motivoIngreso;
	private String diagnostico;
	private String estadoNutricional;
	private String familia;
	private String otros;
	private int idExpediente;

	public NotaHospitalaria(int id, Calendar fecha, String presionArterial,
			String frecuenciaCardiaca, String frecuenciaRespiratoria,
			String motivoIngreso, String diagnostico, String estadoNutricional,
			String familia, String otros, int idExpediente) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.presionArterial = presionArterial;
		this.frecuenciaCardiaca = frecuenciaCardiaca;
		this.frecuenciaRespiratoria = frecuenciaRespiratoria;
		this.motivoIngreso = motivoIngreso;
		this.diagnostico = diagnostico;
		this.estadoNutricional = estadoNutricional;
		this.familia = familia;
		this.otros = otros;
		this.idExpediente = idExpediente;
	}

	public NotaHospitalaria() {
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the fecha
	 */
	public Calendar getFecha() {
		return fecha;
	}
	
	public Date getFechaAsDate() {
		return (Date)fecha.getTime();
	}
	
	public String getFechaFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(fecha.getTime());
	}

	/**
	 * @return the presionArterial
	 */
	public String getPresionArterial() {
		return presionArterial;
	}

	/**
	 * @return the frecuenciaCardiaca
	 */
	public String getFrecuenciaCardiaca() {
		return frecuenciaCardiaca;
	}

	/**
	 * @return the frecuenciaRespiratoria
	 */
	public String getFrecuenciaRespiratoria() {
		return frecuenciaRespiratoria;
	}

	/**
	 * @return the motivoIngreso
	 */
	public String getMotivoIngreso() {
		return motivoIngreso;
	}

	/**
	 * @return the diagnostico
	 */
	public String getDiagnostico() {
		return diagnostico;
	}

	/**
	 * @return the estadoNutricional
	 */
	public String getEstadoNutricional() {
		return estadoNutricional;
	}

	/**
	 * @return the familia
	 */
	public String getFamilia() {
		return familia;
	}

	/**
	 * @return the otros
	 */
	public String getOtros() {
		return otros;
	}

	/**
	 * @return the idExpediente
	 */
	public int getIdExpediente() {
		return idExpediente;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}
	
	public void setFecha(Date fecha){
		this.fecha.setTime(fecha);
	}

	/**
	 * @param presionArterial
	 *            the presionArterial to set
	 */
	public void setPresionArterial(String presionArterial) {
		this.presionArterial = presionArterial;
	}

	/**
	 * @param frecuenciaCardiaca
	 *            the frecuenciaCardiaca to set
	 */
	public void setFrecuenciaCardiaca(String frecuenciaCardiaca) {
		this.frecuenciaCardiaca = frecuenciaCardiaca;
	}

	/**
	 * @param frecuenciaRespiratoria
	 *            the frecuenciaRespiratoria to set
	 */
	public void setFrecuenciaRespiratoria(String frecuenciaRespiratoria) {
		this.frecuenciaRespiratoria = frecuenciaRespiratoria;
	}

	/**
	 * @param motivoIngreso
	 *            the motivoIngreso to set
	 */
	public void setMotivoIngreso(String motivoIngreso) {
		this.motivoIngreso = motivoIngreso;
	}

	/**
	 * @param diagnostico
	 *            the diagnostico to set
	 */
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	/**
	 * @param estadoNutricional
	 *            the estadoNutricional to set
	 */
	public void setEstadoNutricional(String estadoNutricional) {
		this.estadoNutricional = estadoNutricional;
	}

	/**
	 * @param familia
	 *            the familia to set
	 */
	public void setFamilia(String familia) {
		this.familia = familia;
	}

	/**
	 * @param otros
	 *            the otros to set
	 */
	public void setOtros(String otros) {
		this.otros = otros;
	}

	/**
	 * @param idExpediente
	 *            the idExpediente to set
	 */
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}

}
