package modelo.expediente;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Tratamiento {
	private int id;
	private String medicamento;
	private String dosis;
	private String frecuencia;
	private Calendar fechaInicio;
	private Calendar fechaFin;
	private int idExpediente;
	public Tratamiento(int id, String medicamento, String dosis,
			String frecuencia, Calendar fechaInicio, Calendar fechaFin,
			int idExpediente) {
		super();
		this.id = id;
		this.medicamento = medicamento;
		this.dosis = dosis;
		this.frecuencia = frecuencia;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.idExpediente = idExpediente;
	}
	public Tratamiento() {
		super();
		this.fechaFin = Calendar.getInstance();
		this.fechaInicio = Calendar.getInstance();
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the medicamento
	 */
	public String getMedicamento() {
		return medicamento;
	}
	/**
	 * @return the dosis
	 */
	public String getDosis() {
		return dosis;
	}
	/**
	 * @return the frecuencia
	 */
	public String getFrecuencia() {
		return frecuencia;
	}
	/**
	 * @return the fechaInicio
	 */
	public Calendar getFechaInicio() {
		return fechaInicio;
	}
	
	public Date getFechaInicioAsDate() {
		return (Date) fechaInicio.getTime();
	}
	
	public String getFechaInicioFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(fechaInicio.getTime());
	}
	/**
	 * @return the fechaFin
	 */
	public Calendar getFechaFin() {
		return fechaFin;
	}
	
	public Date getFechaFinAsDate() {
		return (Date) fechaFin.getTime();
	}
	
	public String getFechaFinFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(fechaFin.getTime());
	}
	/**
	 * @return the idExpediente
	 */
	public int getIdExpediente() {
		return idExpediente;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param medicamento the medicamento to set
	 */
	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}
	/**
	 * @param dosis the dosis to set
	 */
	public void setDosis(String dosis) {
		this.dosis = dosis;
	}
	/**
	 * @param frecuencia the frecuencia to set
	 */
	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}
	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Calendar fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio.setTime(fechaInicio);
	}
	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(Calendar fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public void setFechaFin(Date fechaFin) {
		this.fechaFin.setTime(fechaFin);
	}
	/**
	 * @param idExpediente the idExpediente to set
	 */
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	
}
