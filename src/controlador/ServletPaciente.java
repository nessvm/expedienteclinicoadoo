package controlador;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.OutputStream;
import java.util.ArrayList;

import modelo.dao.UserDAO;
import modelo.usuario.Medico;
import modelo.usuario.Paciente;

/**
 * Servlet implementation class ServletPaciente
 */
@WebServlet("/ServletPaciente")
public class ServletPaciente extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPaciente() {
        super();
        Context initContext;
        
        try{
        	initContext = new InitialContext();
        	Context envContext = (Context)initContext.lookup("java:/comp/env");
        	ds = (DataSource)envContext.lookup("pool/bd");
        } catch (NamingException e){
        	e.printStackTrace();
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cedProf = request.getParameter("txtCedProf");
		Paciente paciente = (Paciente)request.getSession().getAttribute("user");
		Medico medico = new Medico(ds, cedProf);
		UserDAO uDAO = new UserDAO(ds); 
		ArrayList<Medico> medicos = uDAO.getMedicos(paciente.getCurp());
		
		for (int i = 0; i < medicos.size(); i++){
			if (medicos.get(i).getCedulaProfesional().equals(cedProf)){
				request.getSession().setAttribute("message", "El doctor que intent� agregar ya est� autorizado en su expediente");
				response.sendRedirect("menuPaciente.jsp");
				return;
			}
		}
		
		if (medico.isValido()){
			medicos.add(medico);
			request.getSession().setAttribute("message", "El m�dico que indic� ha sido agregado a su expediente");
			request.getSession().setAttribute("medicos", medicos);
			response.sendRedirect("menuPaciente.jsp");
		}
		
		else {
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String option, actPW, newPW, confNewPW;
		Paciente pac;
		
		option = request.getParameter("Option");
		if ((pac = (Paciente)request.getSession().getAttribute("user")) == null){
			response.sendRedirect("/ExpedienteClinico/index.jsp");
			return;
		}
		
		switch(option){
		
		case "contrase�a":
			actPW = request.getParameter("txtActual");
			newPW = request.getParameter("txtNueva");
			confNewPW = request.getParameter("txtConfirmar");
			
			if(actPW.equals(pac.getContrase�a()) && newPW.equals(confNewPW)){
				pac.setContrase�a(newPW);
				System.out.println("Contrase�a cambiada");
				request.getSession().setAttribute("user", pac);
				response.sendRedirect("/ExpedienteClinico/sub/Paciente/DatosPaciente.jsp");
				return;
			}
			else{
				OutputStream resStream = response.getOutputStream();
				response.setContentType("text/html");
				resStream.write("Ocurri� un error al intentar el cambio de contrase�a".getBytes());
				resStream.close();
				return;
			}
		
		}
		
	}

}
