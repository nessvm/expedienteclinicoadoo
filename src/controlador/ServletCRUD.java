package controlador;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import modelo.usuario.Administrador;
import modelo.usuario.Medico;
import modelo.usuario.Paciente;

/**
 * Servlet implementation class ServletCRUD
 */
@WebServlet("/ServletCRUD")
public class ServletCRUD extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String nombre, apellidoPaterno, apellidoMaterno, calle, numExt, numInt, cp, telefono, correo, userID,
	contraseņa;
	private DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletCRUD() {
		super();
		Context initContext;

		try{
			initContext = new InitialContext();
			Context envContext = (Context)initContext.lookup("java:/comp/env");
			ds = (DataSource)envContext.lookup("pool/bd");
		} catch (NamingException e){
			e.printStackTrace();
		}
	}

	@Override
	public void init() throws ServletException {
		super.init();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userType;
		String admin = null;
		String con = null;
		String btn = request.getParameter("btnAccion");
		if(btn.equals("Registrar"))
		{
			userType = "Administrador";
			admin = request.getParameter("txtUsuario");
			con = request.getParameter("txtContrasena");
		}
		else
			userType = request.getParameter("cbTipoUsr");
		switch(userType){
			case "Paciente":
				getUserParameters(request);
				userID = request.getParameter("txtCURP");
				Paciente nuevoPaciente = new Paciente(ds, userID, contraseņa, nombre,
						apellidoPaterno, apellidoMaterno, calle, numExt, numInt, cp, telefono, correo);
				if (nuevoPaciente.isValido())
					response.sendRedirect("index.jsp");
				else
					response.sendError(0, "Registro fallido");
				break;	
			case "Medico":
				getUserParameters(request);
				userID = request.getParameter("txtCedula");
				Medico nuevoMedico = new Medico(ds, userID, contraseņa, nombre,
						apellidoPaterno, apellidoMaterno, calle, numExt, numInt, cp, telefono, correo, true);
				if (nuevoMedico.isValido())
					response.sendRedirect("index.jsp");
				else
					response.sendError(0, "Registro fallido");
				break;
			case "Administrador":
				Administrador nuevoAdministrador = new Administrador(ds, admin, con);
				if(nuevoAdministrador.isValido())
					response.sendRedirect("index.jsp");
				else
					response.sendError(0,"Registro fallido");
		}
	}

	private void getUserParameters(HttpServletRequest request){
		nombre = request.getParameter("txtNombre");
		apellidoPaterno = request.getParameter("txtApaterno");
		apellidoMaterno = request.getParameter("txtAmaterno");
		calle = request.getParameter("txtCalle");
		numExt = request.getParameter("txtNumExt");
		numInt = request.getParameter("txtNumInt");
		cp = request.getParameter("txtCP");
		telefono = request.getParameter("txtTel");
		correo = request.getParameter("txtEmail");
		contraseņa = request.getParameter("txtContrasena");
	}

}
