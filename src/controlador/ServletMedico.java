package controlador;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import modelo.dao.GenericCRUD;
import modelo.expediente.Consulta;
import modelo.expediente.Expediente;
import modelo.usuario.Medico;
import modelo.usuario.Paciente;

/**
 * Servlet implementation class ServletMedico
 */
@WebServlet("/ServletMedico")
public class ServletMedico extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletMedico() {
        super();
        Context initContext;
        
        try{
        	initContext = new InitialContext();
        	Context envContext = (Context)initContext.lookup("java:/comp/env");
        	ds = (DataSource)envContext.lookup("pool/bd");
        } catch (NamingException e){
        	e.printStackTrace();
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String curp, option;
		HttpSession session = request.getSession();
		Paciente paciente;
		
		curp = request.getParameter("curp");
		option = request.getParameter("option");
		paciente = new Paciente(ds, curp);
		
		if (!paciente.isValido()){
			session.setAttribute("message", "La CURP que ha introducido no pertenece a un paciente registrado");
			response.sendRedirect("ECE/Medico/menuMedico.jsp");
			return;
		}
		
		session.setAttribute("curp", curp);
		switch (option){
		
		case "consulta":
			response.sendRedirect("sub/Medico/consulta.jsp");
			return;
			
		case "notahospital":
			response.sendRedirect("sub/Medico/notahospital.jsp");
			return;
			
		case "notaquirurgica":
			response.sendRedirect("sub/Medico/notacirugia.jsp");
			return;
			
		case "tratamiento":
			response.sendRedirect("sub/Medico/tratamiento.jsp");
			return;
			
		case "expediente":
			response.sendRedirect("sub/Medico/expediente.jsp");
			return;
			
		default:
			session.setAttribute("message", "Ha ocurrido un error, por favor intente nuevamente");
			response.sendRedirect("ECE/Medico/menuMedico.jsp");
			return;
		
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String curp, option = request.getParameter("option");
		Medico med;
		Expediente ex;
		Consulta cons;
		Calendar cal;
		SimpleDateFormat sdf;
		HttpSession session;
		GenericCRUD keeper;
		
		keeper = new GenericCRUD(ds);
		session = request.getSession();
		med = (Medico)session.getAttribute("user");
		curp = (String)session.getAttribute("curp");
		ex = (Expediente)keeper.selectObject(curp, "Expediente");
		switch(option){
		
		case "Consulta":
			cons = new Consulta();
			cons.setCedulaProfesional(med.getCedulaProfesional());
			cons.setDiagnostico(request.getParameter("txtDiag"));
			cal = Calendar.getInstance();
			sdf = new SimpleDateFormat("MM-dd-yyyy");
			try {
				cal.setTime(sdf.parse(request.getParameter("txtFecha")));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			cons.setFecha(cal);
			cons.setIdExpediente(ex.getIdExpediente());
			if (keeper.insertObject(cons) != -1)
				session.setAttribute("message", "La consulta se ha registrado correctamente");
			else
				session.setAttribute("message", "Hubo un error al registrar la consulta");
			response.sendRedirect("ECE/Medico/menuMedico.jsp");
			return;
			
		case "Tratamiento":
			
			return;
		
		}
	}

}
