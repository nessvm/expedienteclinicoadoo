package controlador;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import modelo.dao.UserDAO;
import modelo.usuario.Administrador;
import modelo.usuario.Medico;
import modelo.usuario.Paciente;

/**
 * Servlet implementation class ServletLogin
 */
@WebServlet("/ServletLogin")
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletLogin() {
        super();
        Context initContext;
        
        try{
        	initContext = new InitialContext();
        	Context envContext = (Context)initContext.lookup("java:/comp/env");
        	ds = (DataSource)envContext.lookup("pool/bd");
        } catch (NamingException e){
        	e.printStackTrace();
        }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("txtUsuario");
		String password = request.getParameter("txtContrasena");
		HttpSession session = request.getSession();
		UserDAO uDAO = new UserDAO(ds);
		
		String userType = getUserType(user);
		
		/*
		 * Falta el manejo de una excepci�n en caso de que el usuario y contrase�a introducidos no sean
		 * v�lidos.
		 */
		if (userType == null){
			response.sendRedirect("error.jsp");
		}
		
		if (verifyPassword(user, userType, password)) {
			if(userType.equals("Paciente")){
				session.setAttribute("medicos", uDAO.getMedicos(user));
				session.setAttribute("consultas", uDAO.getConsultas(user));
				session.setAttribute("tratamientos", uDAO.getTratamientos(user));
				session.setAttribute("notasHospital", uDAO.getNotasHospital(user));
				session.setAttribute("notasCirugia", uDAO.getNotasCirugia(user));
				session.setAttribute("user", new Paciente(ds, user));
				session.setMaxInactiveInterval(-1);
				response.sendRedirect("menuPaciente.jsp");
				return;
			}
			else if(userType.equals("Medico")) {
				session.setAttribute("user", new Medico(ds, user));
				session.setMaxInactiveInterval(-1);
			}
			else{
				session.setAttribute("user", new Administrador(ds, user));
				session.setMaxInactiveInterval(-1);
			}
			response.sendRedirect(String.format("ECE/%s/menu%s.jsp", userType, userType));
		}
	}
	
	private String getUserType(String user){
		String[] userType = {"Paciente", "Medico", "Administrador"};
		String[] primaryKey = {"curp", "cedulaProfesional", "idAdministrador"};
		String query;
		ResultSet result;
		
		try{
			Connection con = ds.getConnection();
			Statement sentencia = con.createStatement();
			
			for (int i = 0; i < 3; i++){
				query = String.format("SELECT * FROM ECE.%s WHERE %s = '%s';", userType[i], primaryKey[i], user);
				result = sentencia.executeQuery(query);
				if (result.next()){
					return userType[i];
				}
			}
			con.close();
			
		} catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	private boolean verifyPassword(String user, String userType, String password){
		String query;
		String id;
		Statement statement;
		ResultSet result;
		
		if (userType.equals("Paciente"))
			id = "curp";
		else if (userType.equals("Medico"))
			id = "cedulaProfesional";
		else
			id = "idAdministrador";
		
		try{
			Connection con = ds.getConnection();
			statement = con.createStatement();
			query = String.format("SELECT contrase�a FROM ECE.%s WHERE %s = '%s';",
					userType, id, user);
			result = statement.executeQuery(query);
			
			if (!result.next())
				return false;
			
			if (result.getString("contrase�a").equals(password))
				return true;
			con.close();
			
		} catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}
}
